const isProd = process.argv.includes("--production");
const isDev = !isProd;

export default  {
    isProd: isProd,
    isDev: isDev,

    htmlmin: {
        collapseWhitespace: isProd
    },

    imagemin: {
        verbose: true
    },

    fonter: {
        Formats: ["ttf", "woff", "eot", "svg"]
    }
}