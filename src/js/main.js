const btn = document.querySelector(".navbar-cont-menu")
const navbar = document.querySelector(".navbar-menu")
const overlay = document.querySelector(".navbar-overlay")

btn.addEventListener("click", open)
overlay.addEventListener("click", close)

function open() {
    navbar.classList.toggle("navbar-menu_block")
    btn.classList.toggle("navbar-cont-menu_x")

    if(navbar.classList.contains("navbar-menu_block")){
        overlay.classList.add("navbar-overlay_block")
    } else {
        overlay.classList.remove("navbar-overlay_block")
    }
}

function close() {
    navbar.classList.remove("navbar-menu_block")
    btn.classList.remove("navbar-cont-menu_x")
    overlay.classList.remove("navbar-overlay_block")
}

const parent = document.getElementById("par");

const student = parent.children[0];
const studentName = student.children[0];
const studentWrapp = student.children[1];
const studentBtn = student.children[1].children[3];

const pro = parent.children[1];
const proName = pro.children[0];
const proWrapp = pro.children[1];
const proBtn = pro.children[1].children[3];

const agency = parent.children[2];
const agencyName = agency.children[0];
const agencyWrapp = agency.children[1];
const agencyBtn = agency.children[1].children[3];

const enter = parent.children[3];
const enterName = enter.children[0];
const enterWrapp = enter.children[1];
const enterBtn = enter.children[1].children[3];


student.addEventListener("mouseover", function() {
    student.classList.add("price-blur__item_hover")
    studentName.classList.add("price-blur__item__name_hover")
    studentWrapp.classList.add("price-blur__item__wrapp_hover")
    studentBtn.classList.add("price-blur__item__wrapp__btn_hover")
    // agency.classList.remove("price-blur__item_hover")
    // agencyName.classList.remove("price-blur__item__name_hover")
    // agencyWrapp.classList.remove("price-blur__item__wrapp_hover")
    // agencyBtn.classList.remove("price-blur__item__wrapp__btn_hover")
});

student.addEventListener("mouseout", function() {
    student.classList.remove("price-blur__item_hover")
    studentName.classList.remove("price-blur__item__name_hover")
    studentWrapp.classList.remove("price-blur__item__wrapp_hover")
    studentBtn.classList.remove("price-blur__item__wrapp__btn_hover")
    // agency.classList.add("price-blur__item_hover")
    // agencyName.classList.add("price-blur__item__name_hover")
    // agencyWrapp.classList.add("price-blur__item__wrapp_hover")
    // agencyBtn.classList.add("price-blur__item__wrapp__btn_hover")
});


pro.addEventListener("mouseover", function() {
    pro.classList.add("price-blur__item_hover")
    proName.classList.add("price-blur__item__name_hover")
    proWrapp.classList.add("price-blur__item__wrapp_hover")
    proBtn.classList.add("price-blur__item__wrapp__btn_hover")
    // agency.classList.remove("price-blur__item_hover")
    // agencyName.classList.remove("price-blur__item__name_hover")
    // agencyWrapp.classList.remove("price-blur__item__wrapp_hover")
    // agencyBtn.classList.remove("price-blur__item__wrapp__btn_hover")
});

pro.addEventListener("mouseout", function() {
    pro.classList.remove("price-blur__item_hover")
    proName.classList.remove("price-blur__item__name_hover")
    proWrapp.classList.remove("price-blur__item__wrapp_hover")
    proBtn.classList.remove("price-blur__item__wrapp__btn_hover")
    // agency.classList.add("price-blur__item_hover")
    // agencyName.classList.add("price-blur__item__name_hover")
    // agencyWrapp.classList.add("price-blur__item__wrapp_hover")
    // agencyBtn.classList.add("price-blur__item__wrapp__btn_hover")
});


enter.addEventListener("mouseover", function() {
    enter.classList.add("price-blur__item_hover")
    enterName.classList.add("price-blur__item__name_hover")
    enterWrapp.classList.add("price-blur__item__wrapp_hover")
    enterBtn.classList.add("price-blur__item__wrapp__btn_hover")
    // agency.classList.remove("price-blur__item_hover")
    // agencyName.classList.remove("price-blur__item__name_hover")
    // agencyWrapp.classList.remove("price-blur__item__wrapp_hover")
    // agencyBtn.classList.remove("price-blur__item__wrapp__btn_hover")
});

enter.addEventListener("mouseout", function() {
    enter.classList.remove("price-blur__item_hover")
    enterName.classList.remove("price-blur__item__name_hover")
    enterWrapp.classList.remove("price-blur__item__wrapp_hover")
    enterBtn.classList.remove("price-blur__item__wrapp__btn_hover")
    // agency.classList.add("price-blur__item_hover")
    // agencyName.classList.add("price-blur__item__name_hover")
    // agencyWrapp.classList.add("price-blur__item__wrapp_hover")
    // agencyBtn.classList.add("price-blur__item__wrapp__btn_hover")
});

function fontsize() {
    let block = document.querySelector('.promo-txt-ph');
    let text = document.querySelector('.promo-txt-ph span');
    let w = block.offsetWidth;

    if(w <= 650){
        text.style.fontSize = w/13 + "px";
    } else{
        text.style.fontSize = 48 +"px"
    }
}
window.onload = fontsize;
window.onresize = fontsize; 