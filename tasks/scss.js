import gulp from "gulp"

// Конфигурация
import path from "../config/path.js"
import app from "../config/app.js"

// Плагины
import plumber from "gulp-plumber"
import notify from "gulp-notify"
import size from "gulp-size"
import webpCss from "gulp-webp-css"
import csso from "gulp-csso"
import rename from "gulp-rename"
import groupCssMediaQueries from "gulp-group-css-media-queries"
import shorthand from "gulp-shorthand"
import autoprefixer from "gulp-autoprefixer"
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);


// Обработка SCSS
export default () => {
    return gulp.src(path.scss.src, {sourcemaps: app.isDev})
    .pipe(plumber({
        errorHandler: notify.onError(error => ({
            title: "SCSS",
            message: error.message
        }))
    }))
    .pipe(sass())
    .pipe(webpCss())
    .pipe(autoprefixer())
    .pipe(shorthand())
    .pipe(groupCssMediaQueries())
    .pipe(size({title: "main.css"}))
    .pipe(gulp.dest(path.scss.dest, {sourcemaps: app.isDev}))
    .pipe(rename({suffix: ".min"}))
    .pipe(csso())
    .pipe(size({title: "main.min.css"}))
    .pipe(gulp.dest(path.scss.dest, {sourcemaps: app.isDev}));
};







// Маски выбора файлов: 
// Если нам нужно найти все HTML - return src("./src/html/*.html") 
// Если нужно найти все файлы return src("./src/html/*.*") 
// Если нужны конкретные return src("./src/html/*.{html,css}") 
// Так же работает и с директориями return src("./src/{html,css}/*.{html.css}") 
// Поиск всех файлов в директории src вне зависимости от их вложенности return src("./src/**/*.*"))

// const { src, dest,} = require("gulp")

// // Конфигурация
// const path = require("../config/path.js")
// const app = require("../config/app.js")

// // Плагины
// const sass = require("gulp-sass")(require("sass"));
// const gp = require("gulp-load-plugins")();


// // Обработка SCSS
// const scss = () => {
//     return src(path.scss.src, {sourcemaps: app.isDev})
//     .pipe(gp.plumber({
//         errorHandler: gp.notify.onError(error => ({
//             title: "SCSS",
//             message: error.message
//         }))
//     }))
//     .pipe(sass())
//     .pipe(gp.webpCss())
//     .pipe(gp.autoprefixer())
//     .pipe(gp.shorthand())
//     .pipe(gp.groupCssMediaQueries())
//     .pipe(gp.size({title: "main.css"}))
//     .pipe(dest(path.scss.dest, {sourcemaps: app.isDev}))
//     .pipe(gp.rename({suffix: ".min"}))
//     .pipe(gp.csso())
//     .pipe(gp.size({title: "main.min.css"}))
//     .pipe(dest(path.scss.dest, {sourcemaps: app.isDev}));
// }

// module.exports = scss;