import gulp from "gulp";

// Конфигурация
import path from "../config/path.js";
import app from "../config/app.js";

// Плагины
import plumber from "gulp-plumber";
import notify from "gulp-notify";
import size from "gulp-size";
import concat from "gulp-concat";
import cssimport from "gulp-cssimport";
import webpCss from "gulp-webp-css";
import csso from "gulp-csso";
import rename from "gulp-rename";
import groupCssMediaQueries from "gulp-group-css-media-queries";
import shorthand from "gulp-shorthand";
import autoprefixer from "gulp-autoprefixer";

// Обработка CSS
const css = () => {
    return gulp.src(path.css.src, {sourcemaps: app.isDev})
    .pipe(plumber({
        errorHandler: notify.onError(error => ({
            title: "CSS",
            message: error.message
        }))
    }))
    .pipe(concat("main.css"))
    .pipe(cssimport())
    .pipe(webpCss())
    .pipe(autoprefixer())
    .pipe(shorthand())
    .pipe(groupCssMediaQueries())
    .pipe(size({title: "main.css"}))
    .pipe(gulp.dest(path.css.dest, {sourcemaps: app.isDev}))
    .pipe(rename({suffix: ".min"}))
    .pipe(csso())
    .pipe(size({title: "main.min.css"}))
    .pipe(gulp.dest(path.css.dest, {sourcemaps: app.isDev}));
}





// Маски выбора файлов: 
// Если нам нужно найти все HTML - return src("./src/html/*.html") 
// Если нужно найти все файлы return src("./src/html/*.*") 
// Если нужны конкретные return src("./src/html/*.{html,css}") 
// Так же работает и с директориями return src("./src/{html,css}/*.{html.css}") 
// Поиск всех файлов в директории src вне зависимости от их вложенности return src("./src/**/*.*"))

