import gulp from "gulp"

// Конфигурация
import path from "../config/path.js";
import app from "../config/app.js";

// Плагины
import plumber from "gulp-plumber";
import notify from "gulp-notify";
import size from "gulp-size";
import htmlmin from "gulp-htmlmin";
import fileInclude from "gulp-file-include";
import webpHtml from "gulp-webp-html";

// Обработка HTML 
export default() => {
    return gulp.src(path.html.src)
    .pipe(plumber({
        errorHandler: notify.onError(error => ({
            title: "HTML",
            message: error.message
        }))
    }))
    .pipe(fileInclude())
    .pipe(webpHtml())
    .pipe(size({title: "До сжатия:"}))
    .pipe(htmlmin(app.htmlmin)) 
    .pipe(size({title: "После сжатия:"}))
    .pipe(gulp.dest(path.html.dest));
}







// Маски выбора файлов: 
// Если нам нужно найти все HTML - return src("./src/html/*.html") 
// Если нужно найти все файлы return src("./src/html/*.*") 
// Если нужны конкретные return src("./src/html/*.{html,css}") 
// Так же работает и с директориями return src("./src/{html,css}/*.{html.css}") 
// Поиск всех файлов в директории src вне зависимости от их вложенности return src("./src/**/*.*"))

