// Конфигурация
import {deleteAsync} from "del";

// Плагины
import path from "../config/path.js";

// Удаление директории
export default () => {
    return deleteAsync(path.root)
}
